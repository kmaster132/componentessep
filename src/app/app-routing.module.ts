import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AgregarComponent} from './CRUD/agregar/agregar.component';
import {VerComponent} from './CRUD/ver/ver.component';
import { VerdirectComponent } from './CRUD/verdirect/verdirect.component';

const routes: Routes = [
  {path:'agregar',component:AgregarComponent},
  {path:'ver',component:VerComponent},
  {path:'verdirect',component:VerdirectComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
