import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerdirectComponent } from './verdirect.component';

describe('VerdirectComponent', () => {
  let component: VerdirectComponent;
  let fixture: ComponentFixture<VerdirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerdirectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerdirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
