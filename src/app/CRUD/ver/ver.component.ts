import { Component, OnInit, Input } from '@angular/core';
import { Persona } from './../Objects/Persona';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.component.html',
  styleUrls: ['./ver.component.css']
})
export class VerComponent implements OnInit {
  @Input() persona:Persona[] = []
  
  constructor() { }

  ngOnInit() {

  }

}
