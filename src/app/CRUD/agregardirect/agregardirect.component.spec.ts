import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregardirectComponent } from './agregardirect.component';

describe('AgregardirectComponent', () => {
  let component: AgregardirectComponent;
  let fixture: ComponentFixture<AgregardirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregardirectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregardirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
