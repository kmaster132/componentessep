import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'
import { Persona } from '../Objects/Persona';

@Component({
  selector: 'app-agregardirect',
  templateUrl: './agregardirect.component.html',
  styleUrls: ['./agregardirect.component.css']
})
export class AgregardirectComponent implements OnInit {
  persona:Persona;
  constructor() { }

  ngOnInit() {
  }
  onSubmit(submit:NgForm){
    this.persona = submit.value;
    
    submit.reset();
  
    }
}
