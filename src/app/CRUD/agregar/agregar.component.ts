import { Component, OnInit } from '@angular/core';
import { Persona } from '../Objects/Persona';
import { NgForm } from '@angular/forms'

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {
  public Icono:string;
  public fonts = ["plane","rocket","ship","space-shuttle","subway","taxi","train","truck","wheelchair","wheelchair-alt"];
  persona:Persona;
  personas:Persona[]=[];
  
  constructor() {


  }

  ngOnInit() {
    this.reset();
    
  }
  public reset(){
    this.Icono = "fa fa-"+this.fonts[Math.floor((Math.random() * 10) + 0)];
  }
  onSubmit(submit:NgForm){
  this.persona = submit.value;
  this.personas.push(this.persona)
  submit.reset();
  this.reset();


  }

}
