import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
    private base:string = 'http://localhost:3333'
    constructor(private http:HttpClient) {}
   
    getDatos() {
    return this.http.get(this.base+"/json")
    .subscribe(data =>{
        console.log(data);
    }) 
    }


}