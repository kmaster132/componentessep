import { Injectable, Directive } from '@angular/core';
@Injectable()
export class GlobalCurrencyService {
    private currency;
    constructor() { }
    setCurrency(val) {
        this.currency = val;
    }
    getCurrency() {
        return this.currency;
    }
}